package pixawall.pixawall;

public class Constants {
    // API Keys
    public static final String PIXABAY_KEY = "3727878-0e6e63e7e552d1f1da56fab40";
    public static final String ADS_MOB_KEY = "ca-app-pub-3940256099942544~3347511713";

    // Class Constants
    public static final String LOCK_FILE = "lock.jpg";
    public static final String HOME_FILE = "home.jpg";
    public final static String FILE_AUTHORITY = "pixawall.pixawall.fileprovider";
    public final static String UNKNOWN_USER = "Unknown";
    public final static int JOB_ID = 80738865;

    // Class Generate Preferences
    public final static String PREF_WORKER_ID = "worker_id";
    public final static String PREF_HOME_USERNAME = "home_username";
    public final static String PREF_HOME_PROFILE = "home_profile";
    public final static String PREF_HOME_PAGE = "home_page";
    public final static String PREF_LOCK_USERNAME = "lock_username";
    public final static String PREF_LOCK_PROFILE = "lock_profile";
    public final static String PREF_LOCK_PAGE = "lock_page";
    public final static String PREF_PHOTOS_LIST = "photo_list";
    public final static String PREF_WORK_COUNT = "work_count";
    public final static String PREF_LAST_WORK_STATUS = "last_work_status";

    // XML Generate Preferences
    public final static String PREF_DIFF_PHOTO = "different_photo";
    public final static String PREF_UNSAFE = "unsafe_photo";
    public final static String PREF_TYPE = "type";
    public final static String PREF_ORIENTATION = "orientation";
    public final static String PREF_CATEGORY = "category";
    public final static String PREF_FREQUENCY = "update_frequency";
    public final static String PREF_OWN_API_KEY = "own_api_key";
}

package pixawall.pixawall;

import android.app.WallpaperManager;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Random;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class WallpaperJobService extends JobService {
    private final static String TAG = "WALLPAPER_WORKER";

    private boolean isWorking = false;
    private boolean isCancelled = false;

    private Random random = new Random();

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        Log.d(TAG, "Job started!");
        isWorking = true;

        startWorkOnNewThread(jobParameters);

        return isWorking;
    }

    private void startWorkOnNewThread(final JobParameters jobParameters) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                doWork(jobParameters);
            }
        }).start();
    }

    private void doWork(JobParameters jobParameters) {
        Logger logger = new Logger(TAG);
        logger.addText(android.text.format.DateFormat.format("yyyy-MM-dd HH:mm", new java.util.Date()).toString());
        logger.addText(": ");

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
        SharedPreferences.Editor prefEditor = sharedPref.edit();

        Bitmap bmp;

        String link = generateLink(getApplicationContext(), sharedPref);
        prefEditor.putString(Constants.PREF_PHOTOS_LIST, link);
        prefEditor.apply();

        String data = "{}";
        int responseCode = -1;
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder().url(link).get().build();
        Response response = null;

        try {
            response = client.newCall(request).execute();
            responseCode = response.code();

            if (responseCode == 200) {
                final ResponseBody body = response.body();
                data = body.string();
            }
        } catch (Exception e) {
            Log.e(TAG, "Response Code: " + responseCode + "; " + e.getMessage());
            logger.addError("Unable to get JSON, responded code: " + responseCode + "; ");
        } finally {
            if (response != null) {
                response.close();
            }
        }

        try {

            JSONObject json = new JSONObject(data);

            JSONArray items = json.getJSONArray("hits");

            int totalItems = items.length();
            int homeIndex;
            int lockIndex;

            String template = "https://pixabay.com/en/users/%s-%s";
            String userName;
            String userId;
            File file;

            if (sharedPref.getBoolean(Constants.PREF_DIFF_PHOTO, Boolean.parseBoolean(getResources().getString(R.string.pref_general_diff_photo_default)))) {
                homeIndex = random.nextInt(totalItems);

                do {
                    lockIndex = random.nextInt(totalItems);
                } while (homeIndex == lockIndex);

                if (downloadPhoto(items.getJSONObject(lockIndex).getString("largeImageURL"), Constants.LOCK_FILE)) {
                    // Lock Screen Wallpaper

                    userName = items.getJSONObject(lockIndex).getString("user");
                    userId = items.getJSONObject(lockIndex).getString("user_id");
                    prefEditor.putString(Constants.PREF_LOCK_USERNAME, userName);
                    prefEditor.putString(Constants.PREF_LOCK_PAGE, items.getJSONObject(lockIndex).getString("pageURL"));
                    prefEditor.putString(Constants.PREF_LOCK_PROFILE, String.format(template, userName, userId));
                    prefEditor.apply();

                    file = new File(getApplicationContext().getFilesDir(), Constants.LOCK_FILE);
                    if (file.exists()) {
                        bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
                        try {
                            if (Build.VERSION.SDK_INT >= 24) {
                                wallpaperManager.setBitmap(bmp, null, true, WallpaperManager.FLAG_LOCK);
                            } else {
                                wallpaperManager.setBitmap(bmp);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                            logger.addError("Unable to set Lock Screen Wallpaper; ");
                        }
                    }
                } else {
                    logger.addError("Unable to download Lock Screen Photo; ");
                }

                if (downloadPhoto(items.getJSONObject(homeIndex).getString("largeImageURL"), Constants.HOME_FILE)) {
                    // Home Screen Wallpaper

                    userName = items.getJSONObject(homeIndex).getString("user");
                    userId = items.getJSONObject(homeIndex).getString("user_id");
                    prefEditor.putString(Constants.PREF_HOME_USERNAME, userName);
                    prefEditor.putString(Constants.PREF_HOME_PAGE, items.getJSONObject(homeIndex).getString("pageURL"));
                    prefEditor.putString(Constants.PREF_HOME_PROFILE, String.format(template, userName, userId));
                    prefEditor.apply();

                    file = new File(getApplicationContext().getFilesDir(), Constants.HOME_FILE);
                    if (file.exists()) {
                        bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
                        try {
                            if (Build.VERSION.SDK_INT >= 24) {
                                wallpaperManager.setBitmap(bmp, null, true, WallpaperManager.FLAG_SYSTEM);
                            } else {
                                wallpaperManager.setBitmap(bmp);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                            logger.addError("Unable to set Home Screen Wallpaper; ");
                        }
                    }
                } else {
                    logger.addError("Unable to download Home Screen Wallpaper; ");
                }
            } else {
                // Both Home & Lock Screen wallpaper

                homeIndex = random.nextInt(totalItems);

                if (downloadPhoto(items.getJSONObject(homeIndex).getString("largeImageURL"), Constants.HOME_FILE)) {
                    userName = items.getJSONObject(homeIndex).getString("user");
                    userId = items.getJSONObject(homeIndex).getString("user_id");
                    prefEditor.putString(Constants.PREF_HOME_USERNAME, userName);
                    prefEditor.putString(Constants.PREF_HOME_PAGE, items.getJSONObject(homeIndex).getString("pageURL"));
                    prefEditor.putString(Constants.PREF_HOME_PROFILE, String.format(template, userName, userId));
                    prefEditor.apply();

                    file = new File(getApplicationContext().getFilesDir(), Constants.HOME_FILE);
                    if (file.exists()) {
                        bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
                        try {
                            wallpaperManager.setBitmap(bmp);
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                            logger.addError("Unable to set Wallpaper; ");
                        }
                    }
                } else {
                    logger.addError("Unable to download Wallpaper; ");
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "JSON Error: " + e.getMessage());
            logger.addError("Unable to process JSON data; ");
        }

        if (!logger.isError()) {
            logger.addText("Everything OK");
        }

        prefEditor.putInt(Constants.PREF_WORK_COUNT, sharedPref.getInt(Constants.PREF_WORK_COUNT, 0) + 1);
        prefEditor.putString(Constants.PREF_LAST_WORK_STATUS, logger.toString());
        prefEditor.apply();

        Log.i(TAG, logger.toString());
        isWorking = false;
        jobFinished(jobParameters, true);
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        Log.d(TAG, "Job cancelled before being completed.");
        isCancelled = true;
        jobFinished(jobParameters, true);

        return true;
    }

    private String generateLink(Context context, SharedPreferences pref) {
        StringBuilder sb = new StringBuilder("https://pixabay.com/api/?editors_choice=true");

        String apiKey = pref.getString(Constants.PREF_OWN_API_KEY, null);

        sb.append("&key=");

        if (apiKey == null || apiKey.equals("")) {
            sb.append(Constants.PIXABAY_KEY);
        } else {
            sb.append(apiKey);
        }

        sb.append("&image_type=");
        sb.append(pref.getString(Constants.PREF_TYPE, context.getResources().getString(R.string.pref_general_type_default)));

        sb.append("&orientation=");
        sb.append(pref.getString(Constants.PREF_ORIENTATION, context.getResources().getString(R.string.pref_general_orientation_default)));

        String category =pref.getString(Constants.PREF_CATEGORY, context.getResources().getString(R.string.pref_general_category_default));
        if (!category.equals("every")) {
            sb.append("&category=");
            sb.append(category);
        }

        sb.append("&safesearch=");
        sb.append(!pref.getBoolean(Constants.PREF_UNSAFE, Boolean.parseBoolean(context.getResources().getString(R.string.pref_general_unsafe_photo_default))));

        sb.append("&per_page=100");

        sb.append("&page=1");

        return sb.toString();
    }

    private boolean downloadPhoto(String link, String filename) {
        boolean success = false;

        if (isCancelled) {
            return false;
        }

        try {
            URL url = new URL(link);
            InputStream inputStream = new BufferedInputStream(url.openStream(), 8192);

            FileOutputStream outputStream = openFileOutput(filename, Context.MODE_PRIVATE);

            byte data[] = new byte[1024];
            int count;

            while ((count = inputStream.read(data)) != -1) {
                outputStream.write(data, 0, count);
                success = true;
            }

            outputStream.flush();

            outputStream.close();
            inputStream.close();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        Log.i(TAG, link + " => " + filename);

        return success;
    }
}

package pixawall.pixawall;

import android.util.Log;

public class Logger {
    private boolean error = false;
    private String tag;
    private StringBuilder sb;

    public Logger(String tag) {
        this.tag = tag;
        sb = new StringBuilder();
    }

    public void addText(String msg) {
        sb.append(msg);
    }

    public void addError(String msg, boolean display) {
        error = true;

        sb.append(msg);
        if (display) {
            Log.e(tag, msg);
        }
    }
    public void addError(String msg) {
        this.addError(msg, false);
    }

    public void addInfo(String msg, boolean display) {
        sb.append(msg);
        if (display) {
            Log.i(tag, msg);
        }
    }
    public void addInfo(String msg) {
        addInfo(msg, false);
    }

    public void addDebug(String msg, boolean display) {
        sb.append(msg);
        if (display) {
            Log.d(tag, msg);
        }
    }
    public void addDebug(String msg) {
        addDebug(msg, false);
    }

    @Override
    public String toString() {
        return sb.toString();
    }

    public boolean isError() {
        return error;
    }
}

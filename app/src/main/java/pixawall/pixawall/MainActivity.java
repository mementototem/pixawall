package pixawall.pixawall;

import android.annotation.SuppressLint;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.File;
import java.util.List;

import static android.support.v4.content.FileProvider.getUriForFile;
import static java.lang.Integer.parseInt;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = "PIXAWALL_MAIN";

    protected AdView mAdView;

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor prefEditor;
    private ImageView lockImage;
    private ImageView homeImage;
    private CardView lockCard;
    private Button homeProfile;
    private Button lockProfile;

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup Ads
        MobileAds.initialize(this, Constants.ADS_MOB_KEY);
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        prefEditor = sharedPref.edit();

        // Load current wallpapers (by app) to ImageView
        homeImage = findViewById(R.id.home_image);
        lockImage = findViewById(R.id.lock_image);
        lockCard = findViewById(R.id.lock_wallpaper);
        homeProfile = findViewById(R.id.home_profile);
        lockProfile = findViewById(R.id.lock_profile);

        createJob();

        sharedPref.registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (key.equals(Constants.PREF_LAST_WORK_STATUS)) {
                    updateView();
                }
            }
        });

        //TODO: automated version https://medium.com/@manas/manage-your-android-app-s-versioncode-versionname-with-gradle-7f9c5dcf09bf
    }

    @Override
    public void onResume() {
        super.onResume();

        updateView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        if (sharedPref.getString(Constants.PREF_OWN_API_KEY, "").equals("")) {
            menu.findItem(R.id.action_refresh).setVisible(false);
        } else {
            menu.findItem(R.id.action_refresh).setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_setting:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;

            case R.id.action_refresh:
                prefEditor.remove(Constants.PREF_WORKER_ID);
                prefEditor.apply();
                createJob();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateView() {
        if (sharedPref.getBoolean(Constants.PREF_DIFF_PHOTO, true)) {
            setPhotos("LOCK");
            setPhotos("HOME");
            lockCard.setVisibility(View.VISIBLE);
        } else {
            lockCard.setVisibility(View.INVISIBLE);
            setPhotos("BOTH");
        }

        updateProfileButtonText();
    }

    private void setPhotos(String state) {
        Bitmap bmp;
        File file;

        switch (state) {
            case "HOME":
                file = new File(getFilesDir(), Constants.HOME_FILE);
                if (file.exists()) {
                    bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
                    homeImage.setImageBitmap(bmp);
                    homeImage.setAdjustViewBounds(true);
                }
            case "LOCK":
                file = new File(getFilesDir(), Constants.LOCK_FILE);
                if (file.exists()) {
                    bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
                    lockImage.setImageBitmap(bmp);
                    lockImage.setAdjustViewBounds(true);
                }
                break;

            case "BOTH":
                file = new File(getFilesDir(), Constants.HOME_FILE);
                if (file.exists()) {
                    bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
                    homeImage.setImageBitmap(bmp);
                    homeImage.setAdjustViewBounds(true);
                }
                break;
        }
    }

    private void openWeb(String link) {
        if (link == null) {
            return;
        }

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(link));
        startActivity(i);
    }

    private void updateProfileButtonText() {
        if (sharedPref.getBoolean(Constants.PREF_DIFF_PHOTO, true)) {
            homeProfile.setText(String.format(getResources().getString(R.string.main_card_user), sharedPref.getString(Constants.PREF_HOME_USERNAME, Constants.UNKNOWN_USER)));
            lockProfile.setText(String.format(getResources().getString(R.string.main_card_user), sharedPref.getString(Constants.PREF_LOCK_USERNAME, Constants.UNKNOWN_USER)));
        } else {
            homeProfile.setText(String.format(getResources().getString(R.string.main_card_user), sharedPref.getString(Constants.PREF_HOME_USERNAME, Constants.UNKNOWN_USER)));
        }
    }

    private void shareImage(String filename) {
        File file = new File(getApplicationContext().getFilesDir(), filename);
        Uri uri = getUriForFile(getApplicationContext(), Constants.FILE_AUTHORITY, file);

        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType("image/jpeg");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.main_card_share)));
    }

    private void createJob() {
        // if have worker and same interval skip to create new worker
        int interval = parseInt(
                sharedPref.getString(
                        Constants.PREF_FREQUENCY,
                        getResources().getString(R.string.pref_general_frequency_default)
                )
        );

        long intervalMillis = interval * 60 * 60 * 1000; // hours * minutes * seconds * 1000;

        JobScheduler jobScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        JobInfo jobInfo = null;
        
        if (Build.VERSION.SDK_INT >= 24) {
            jobInfo = jobScheduler.getPendingJob(Constants.JOB_ID);
        } else {
            List<JobInfo> jobList = jobScheduler.getAllPendingJobs();
            for (JobInfo job : jobList) {
                if (job.getId() == Constants.JOB_ID) {
                    jobInfo = job;
                    break;
                }
            }
        }

        if (jobInfo == null || jobInfo.getIntervalMillis() != intervalMillis) {
            ComponentName componentName = new ComponentName(this, WallpaperJobService.class);

            jobInfo = new JobInfo.Builder(Constants.JOB_ID, componentName)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setBackoffCriteria(300000, JobInfo.BACKOFF_POLICY_LINEAR)
                    .setPeriodic(intervalMillis)
                    .setPersisted(true)
                    .build();

            int resultCode = jobScheduler.schedule(jobInfo);

            if (resultCode == JobScheduler.RESULT_SUCCESS) {
                Log.i(TAG, "Job was scheduled");
            } else {
                Log.e(TAG, "Unable to schedule job");
            }
        } else {
            Log.i(TAG, "Current job is running as same interval, no need to create the new one");
        }
    }

    public void homeProfileClick(View v) {
        openWeb(sharedPref.getString(Constants.PREF_HOME_PROFILE, null));
    }

    public void homePixabayClick(View v) {
        openWeb(sharedPref.getString(Constants.PREF_HOME_PAGE, null));
    }

    public void homeShareClick(View v) {
        shareImage(Constants.HOME_FILE);
    }

    public void lockProfileClick(View v) {
        openWeb(sharedPref.getString(Constants.PREF_LOCK_PROFILE, null));
    }

    public void lockPixabayClick(View v) {
        openWeb(sharedPref.getString(Constants.PREF_LOCK_PAGE, null));
    }

    public void lockShareClick(View v) {
        shareImage(Constants.LOCK_FILE);
    }
}
